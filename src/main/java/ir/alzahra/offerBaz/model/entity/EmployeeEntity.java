package ir.alzahra.offerBaz.model.entity;

import javax.persistence.*;
import java.util.List;

/**
 * @Author: sobhan_ssh
 **/

@Entity
@Table(name = "EMPLOYEE_ENTITY")
public class EmployeeEntity {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Long id;

    @Column(name = "PERSONAL_CODE")
    private Long personalCode;

    @Column(name = "UNI_NAME")
    private String uniName;

    @Column(name = "NAME")
    private String name;

    @Column(name = "FAMILY_NAME")
    private String familyName;

    @Column(name = "NATIONAL_CODE")
    private Long nationalCode;

    @Column(name = "PHONE")
    private Long phone;

    @Column(name = "ADDRESS")
    private String address;

    @Column(name = "POSTAL_CODE")
    private Long postalCode;

    @Column(name = "TYPE")
    private String type;

    @Column(name = "RESPONSIBILITY")
    private String responsibility;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "EMPLOYEE_ID")
    private List<EnterExitEntity> enterExitEntities;

    /*@OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "EMPLOYEE_ID")
    private List<EmployeeFoodEntity> employeeFoodEntities;*/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPersonalCode() {
        return personalCode;
    }

    public void setPersonalCode(Long personalCode) {
        this.personalCode = personalCode;
    }

    public String getUniName() {
        return uniName;
    }

    public void setUniName(String uniName) {
        this.uniName = uniName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public Long getNationalCode() {
        return nationalCode;
    }

    public void setNationalCode(Long nationalCode) {
        this.nationalCode = nationalCode;
    }

    public Long getPhone() {
        return phone;
    }

    public void setPhone(Long phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(Long postalCode) {
        this.postalCode = postalCode;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getResponsibility() {
        return responsibility;
    }

    public void setResponsibility(String responsibility) {
        this.responsibility = responsibility;
    }

    public List<EnterExitEntity> getEnterExitEntities() {
        return enterExitEntities;
    }

    public void setEnterExitEntities(List<EnterExitEntity> enterExitEntities) {
        this.enterExitEntities = enterExitEntities;
    }

    /*public List<EmployeeFoodEntity> getEmployeeFoodEntities() {
        return employeeFoodEntities;
    }

    public void setEmployeeFoodEntities(List<EmployeeFoodEntity> employeeFoodEntities) {
        this.employeeFoodEntities = employeeFoodEntities;
    }*/
}
