package ir.alzahra.offerBaz.model.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * @Author: sobhan_ssh
 **/

@Entity
@Table(name = "BUY_GOODS_ENTITY")
public class BuyGoodsEntity {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Long id;

    @Column(name = "DATE")
    private Date date;

    @Column(name = "BARCODE_GOOD")
    private Long barcodeGood;

    @Column(name = "NAME_UNIT")
    private String nameUnit;

    @ManyToOne(fetch = FetchType.LAZY)
    private GoodsEntity goodsEntity;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Long getBarcodeGood() {
        return barcodeGood;
    }

    public void setBarcodeGood(Long barcodeGood) {
        this.barcodeGood = barcodeGood;
    }

    public String getNameUnit() {
        return nameUnit;
    }

    public void setNameUnit(String nameUnit) {
        this.nameUnit = nameUnit;
    }

    public GoodsEntity getGoodsEntity() {
        return goodsEntity;
    }

    public void setGoodsEntity(GoodsEntity goodsEntity) {
        this.goodsEntity = goodsEntity;
    }
}
