package ir.alzahra.offerBaz.model.entity;


import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * @Author: sobhan_ssh
 **/


@Entity
@Table(name = "FOOD_ENTITY")
public class FoodEntity {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Long id;

    @Column(name = "DATE")
    @Temporal(TemporalType.DATE)
    private Date date;

    @Column(name = "MEAL")
    private String meal;

    @Column(name = "NAME")
    private String name;

    @Column(name = "NUMBER")
    private Long number;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getMeal() {
        return meal;
    }

    public void setMeal(String meal) {
        this.meal = meal;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

}
