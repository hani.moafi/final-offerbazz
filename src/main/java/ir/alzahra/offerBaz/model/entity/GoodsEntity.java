package ir.alzahra.offerBaz.model.entity;

import javax.persistence.*;
import java.util.List;

/**
 * @Author: sobhan_ssh
 **/


@Entity
@Table(name = "GOODS_ENTITY")
public class GoodsEntity {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Long id;

    @Column(name = "BARCODE")
    private Long barcode;

    @Column(name = "NAME")
    private String name;

    @Column(name = "NUMBER")
    private Long number;


    @Column(name = "UNIT_NAME")
    private String unitName;

/*    @OneToMany(cascade = CascadeType.ALL)
    private List<GoodsConsumptionEntity> goodsConsumptionEntities;*/

    /*@OneToMany(cascade = CascadeType.ALL)
    private List<BuyGoodsEntity> buyGoodsEntities;*/

    @ManyToOne(fetch = FetchType.LAZY)
    private UnitEntity unitEntity;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBarcode() {
        return barcode;
    }

    public void setBarcode(Long barcode) {
        this.barcode = barcode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

/*    public List<GoodsConsumptionEntity> getGoodsConsumptionEntities() {
        return goodsConsumptionEntities;
    }

    public void setGoodsConsumptionEntities(List<GoodsConsumptionEntity> goodsConsumptionEntities) {
        this.goodsConsumptionEntities = goodsConsumptionEntities;
    }*/

  /*  public List<BuyGoodsEntity> getBuyGoodsEntities() {
        return buyGoodsEntities;
    }

    public void setBuyGoodsEntities(List<BuyGoodsEntity> buyGoodsEntities) {
        this.buyGoodsEntities = buyGoodsEntities;
    }
*/
    public UnitEntity getUnitEntity() {
        return unitEntity;
    }

    public void setUnitEntity(UnitEntity unitEntity) {
        this.unitEntity = unitEntity;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }
}
