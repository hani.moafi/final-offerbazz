package ir.alzahra.offerBaz.model.entity;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.util.List;

/**
 * @Author: sobhan_ssh
 **/

@Entity
@Table(name = "UNIT_ENTITY")
public class UnitEntity {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME")
    @NotEmpty(message = "نام سازمان نمیتواند خالی باشد")
    private String name;

    @Column(name = "UNIT_NUMBER")
    private Long unitNumber;

    @Column(name = "DUTIES")
    private String duties;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "UNIT_ID")
    private List<EmployeeEntity> employeeEntities;

    @OneToMany(cascade = CascadeType.ALL,mappedBy = "unitEntity",orphanRemoval = true)
    private List<ToolsEntity> toolsEntities;

    @OneToMany(cascade = CascadeType.ALL,mappedBy = "sendUnitEntity",orphanRemoval = true)
    private List<MailEntity> sendEmails;

    @OneToMany(cascade = CascadeType.ALL,mappedBy = "recievUnitEntity",orphanRemoval = true)
    private List<MailEntity> recieveEmail;


   /* @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "UNIT_ID")
    private List<BuyGoodsEntity> buyGoodsEntities;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "UNIT_ID")
    private List<GoodsConsumptionEntity> goodsConsumptionEntities;*/

 /*   @OneToMany(cascade = CascadeType.ALL)
    private List<GoodsEntity> goods;*/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getUnitNumber() {
        return unitNumber;
    }

    public void setUnitNumber(Long unitNumber) {
        this.unitNumber = unitNumber;
    }

    public String getDuties() {
        return duties;
    }

    public void setDuties(String duties) {
        this.duties = duties;
    }

    public List<EmployeeEntity> getEmployeeEntities() {
        return employeeEntities;
    }

    public void setEmployeeEntities(List<EmployeeEntity> employeeEntities) {
        this.employeeEntities = employeeEntities;
    }

    public List<ToolsEntity> getToolsEntities() {
        return toolsEntities;
    }

    public void setToolsEntities(List<ToolsEntity> toolsEntities) {
        this.toolsEntities = toolsEntities;
    }

/*
    public List<BuyGoodsEntity> getBuyGoodsEntities() {
        return buyGoodsEntities;
    }

    public void setBuyGoodsEntities(List<BuyGoodsEntity> buyGoodsEntities) {
        this.buyGoodsEntities = buyGoodsEntities;
    }

    public List<GoodsConsumptionEntity> getGoodsConsumptionEntities() {
        return goodsConsumptionEntities;
    }

    public void setGoodsConsumptionEntities(List<GoodsConsumptionEntity> goodsConsumptionEntities) {
        this.goodsConsumptionEntities = goodsConsumptionEntities;
    }*/

    public List<MailEntity> getSendEmails() {
        return sendEmails;
    }

    public void setSendEmails(List<MailEntity> sendEmails) {
        this.sendEmails = sendEmails;
    }

    public List<MailEntity> getRecieveEmail() {
        return recieveEmail;
    }

    public void setRecieveEmail(List<MailEntity> recieveEmail) {
        this.recieveEmail = recieveEmail;
    }

 /*   public List<GoodsEntity> getGoods() {
        return goods;
    }

    public void setGoods(List<GoodsEntity> goods) {
        this.goods = goods;
    }*/
}
