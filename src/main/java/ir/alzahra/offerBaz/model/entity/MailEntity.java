package ir.alzahra.offerBaz.model.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * @Author: sobhan_ssh
 **/

@Entity
@Table(name = "MAIL_ENTITY")
public class MailEntity {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Long id;

    @Column(name = "SEND_UNI_NAME")
    private String sendUnitName;

    @Column(name = "RECEIVE_UNI_NAME")
    private String receiveUnitName;

    @Column(name = "MAIL_SERIAL")
    private Long mailSerial;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "TEXT")
    private String text;

    @Column(name = "DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;


    @ManyToOne(fetch = FetchType.LAZY)
    private UnitEntity sendUnitEntity;

    @ManyToOne(fetch = FetchType.LAZY)
    private UnitEntity recievUnitEntity;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSendUnitName() {
        return sendUnitName;
    }

    public void setSendUnitName(String sendUnitName) {
        this.sendUnitName = sendUnitName;
    }

    public String getReceiveUnitName() {
        return receiveUnitName;
    }

    public void setReceiveUnitName(String receiveUnitName) {
        this.receiveUnitName = receiveUnitName;
    }

    public Long getMailSerial() {
        return mailSerial;
    }

    public void setMailSerial(Long mailSerial) {
        this.mailSerial = mailSerial;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public UnitEntity getSendUnitEntity() {
        return sendUnitEntity;
    }

    public void setSendUnitEntity(UnitEntity sendUnitEntity) {
        this.sendUnitEntity = sendUnitEntity;
    }

    public UnitEntity getRecievUnitEntity() {
        return recievUnitEntity;
    }

    public void setRecievUnitEntity(UnitEntity recievUnitEntity) {
        this.recievUnitEntity = recievUnitEntity;
    }
}
