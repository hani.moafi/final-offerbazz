package ir.alzahra.offerBaz.model.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * @Author: sobhan_ssh
 **/

@Entity
@Table(name = "TOOLS_ENTITY")
public class ToolsEntity {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Long id;

    @Column(name = "BARCODE")
    private Long barcode;

    @Column(name = "UNI_NAME")
    private String uniName;

    @Column(name = "NAME")
    private String name;

    @Column(name = "MANUFACTURE_DATE")
    private Date manufactureDate;

    @ManyToOne(fetch = FetchType.LAZY)
    private UnitEntity unitEntity;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBarcode() {
        return barcode;
    }

    public void setBarcode(Long barcode) {
        this.barcode = barcode;
    }

    public String getUniName() {
        return uniName;
    }

    public void setUniName(String uniName) {
        this.uniName = uniName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getManufactureDate() {
        return manufactureDate;
    }

    public void setManufactureDate(Date manufactureDate) {
        this.manufactureDate = manufactureDate;
    }

    public UnitEntity getUnitEntity() {
        return unitEntity;
    }

    public void setUnitEntity(UnitEntity unitEntity) {
        this.unitEntity = unitEntity;
    }
}
