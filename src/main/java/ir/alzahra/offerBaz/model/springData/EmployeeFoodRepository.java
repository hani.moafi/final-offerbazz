package ir.alzahra.offerBaz.model.springData;

import ir.alzahra.offerBaz.model.entity.EmployeeFoodEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;

/**
 * @Author: sobhan_ssh
 **/

@Repository
public interface EmployeeFoodRepository extends CrudRepository<EmployeeFoodEntity, Long> {

    EmployeeFoodEntity findByDateAndMealAndPersonalCode(Date date,String meal,Long PersonalCode);
}
