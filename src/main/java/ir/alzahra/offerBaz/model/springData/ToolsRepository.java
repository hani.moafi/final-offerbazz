package ir.alzahra.offerBaz.model.springData;

import ir.alzahra.offerBaz.model.entity.ToolsEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @Author: sobhan_ssh
 **/

@Repository
public interface ToolsRepository extends CrudRepository<ToolsEntity, Long> {
    ToolsEntity findByBarcode(Long barcode1);
}
