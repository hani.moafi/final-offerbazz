package ir.alzahra.offerBaz.model.springData;

import ir.alzahra.offerBaz.model.entity.EmployeeEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author: sobhan_ssh
 **/

@Repository
public interface EmployeeRepository extends CrudRepository<EmployeeEntity, Long> {

    List<EmployeeEntity> findByPersonalCode(Long personalCode);
}
