package ir.alzahra.offerBaz.model.springData;

import ir.alzahra.offerBaz.model.entity.MailEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @Author: sobhan_ssh
 **/

@Repository
public interface MailRepository extends CrudRepository<MailEntity, Long> {
    MailEntity findByMailSerial(Long serialMail);
}
