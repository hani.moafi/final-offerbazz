package ir.alzahra.offerBaz.model.springData;

import ir.alzahra.offerBaz.model.entity.EnterExitEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * @Author: sobhan_ssh
 **/

@Repository
public interface EnterExitRepository extends CrudRepository<EnterExitEntity, Long> {

    List<EnterExitEntity> findByPersonalCode(Long personalCode);

    EnterExitEntity findByDateAndPersonalCode(Date date, Long personalCode);
}
