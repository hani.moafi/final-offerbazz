package ir.alzahra.offerBaz.model.springData;

import ir.alzahra.offerBaz.model.entity.FoodEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;

/**
 * @Author: sobhan_ssh
 **/

@Repository
public interface FoodRepository extends CrudRepository<FoodEntity, Long> {

    FoodEntity findByName(String name);

    FoodEntity findByDateAndMeal(Date date, String meal);
}
