package ir.alzahra.offerBaz.model.springData;

import ir.alzahra.offerBaz.model.entity.BuyGoodsEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;

/**
 * @Author: sobhan_ssh
 **/

@Repository
public interface BuyGoodsRepository extends CrudRepository<BuyGoodsEntity, Long> {
    BuyGoodsEntity findByDate(Date date);
}
