package ir.alzahra.offerBaz.model.springData;

import ir.alzahra.offerBaz.model.entity.GoodsEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author: sobhan_ssh
 **/

@Repository
public interface GoodsRepository extends CrudRepository<GoodsEntity, Long> {
    GoodsEntity findByBarcode(Long barcode1);

    List<GoodsEntity> findAll();
    List<GoodsEntity> findByUnitName(String unitName);
}
