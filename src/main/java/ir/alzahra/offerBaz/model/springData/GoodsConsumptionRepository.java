package ir.alzahra.offerBaz.model.springData;

import ir.alzahra.offerBaz.model.entity.GoodsConsumptionEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;

/**
 * @Author: sobhan_ssh
 **/

@Repository
public interface GoodsConsumptionRepository extends CrudRepository<GoodsConsumptionEntity, Long> {
    GoodsConsumptionEntity findByDate(Date date);
}
