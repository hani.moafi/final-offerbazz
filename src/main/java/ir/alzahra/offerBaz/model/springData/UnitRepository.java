package ir.alzahra.offerBaz.model.springData;

import ir.alzahra.offerBaz.model.entity.UnitEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author: sobhan_ssh
 **/

@Repository
public interface UnitRepository extends CrudRepository<UnitEntity, Long> {

    List<UnitEntity> findAll();


    boolean existsByName(String name);

    List<UnitEntity> findByName(String name);

    UnitEntity findByUnitNumber(Long unitNumber);
}
