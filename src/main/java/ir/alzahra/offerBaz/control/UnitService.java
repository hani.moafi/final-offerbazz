package ir.alzahra.offerBaz.control;

import ir.alzahra.offerBaz.exception.BaseException;
import ir.alzahra.offerBaz.model.entity.*;

import java.util.Date;
import java.util.List;

/**
 * @Author: hanieh Moafi
 * @Date: 1/18/2020
 **/
public interface UnitService {


    void insertUnit(UnitEntity unitEntity) throws BaseException;

    UnitEntity searchUnit(Long code) throws BaseException;

    public void editUnit(UnitEntity unitEntity) throws BaseException;

    public void deleteUnit(UnitEntity unitEntity) throws BaseException;

    void insertEmployee(EmployeeEntity employee1) throws BaseException;

    EmployeeEntity searchEmployee(String personalCode) throws BaseException;

    void deleteEmployee(EmployeeEntity employee3) throws BaseException;

    void EditEmployee(EmployeeEntity employee3) throws BaseException;

    List<UnitEntity> getALLUnit() throws BaseException;

    void insertFood(FoodEntity foodEntity) throws BaseException;

    FoodEntity searchFood(Date date, String meal) throws BaseException;

    FoodEntity searchFoodByDateAndMeal(Date date, String meal) throws BaseException;

    void deleteFood(FoodEntity foodEntity) throws BaseException;

    void EditFood(FoodEntity foodEntity) throws BaseException;

    void insertTools(ToolsEntity toolsEntity) throws BaseException;

    ToolsEntity findToolByBarcode(Long barcode1) throws BaseException;

    void editTools(ToolsEntity toolsForEdit) throws BaseException;

    void deleteTool(ToolsEntity toolsForEdit) throws BaseException;

    void sendMail(MailEntity mailEntity) throws BaseException;

    MailEntity findMailBySerial(Long serialMail) throws BaseException;

    void deleteMail(MailEntity mailEntity3) throws BaseException;

    void insertGoods(GoodsEntity goodsEntity) throws BaseException;

    GoodsEntity searchGoodByBarcode(Long barcode1) throws BaseException;

    void deleteGood(GoodsEntity goodsEntity3) throws BaseException;

    void editGood(GoodsEntity goodsEntity3) throws BaseException;

    List<GoodsEntity> findGoodOfUnit(String nameUnit) throws BaseException;

    void insertEnterExit(EnterExitEntity enterExitEntity) throws BaseException;

    EnterExitEntity searchEnterExit(Date date,Long personalCode) throws BaseException;

    void deleteEnterExit(EnterExitEntity enterExitEntity) throws BaseException;

    void editEnterExit(EnterExitEntity enterExitEntity) throws BaseException;

    void insertBuyGoods(BuyGoodsEntity buyGoodsEntity) throws BaseException;

    void insertGoodConsumption(GoodsConsumptionEntity goodsConsumptionEntity) throws BaseException;

    void insertEmployeeFood(EmployeeFoodEntity employeeFoodEntity) throws BaseException;

    EmployeeFoodEntity searchEmployeeFood(Date date, String meal,Long code) throws BaseException;

    void deleteEmployeeFood(EmployeeFoodEntity employeeFoodEntity) throws BaseException;

}
