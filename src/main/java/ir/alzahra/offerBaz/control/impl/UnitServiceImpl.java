package ir.alzahra.offerBaz.control.impl;

import ir.alzahra.offerBaz.control.UnitService;
import ir.alzahra.offerBaz.exception.BaseException;
import ir.alzahra.offerBaz.model.entity.*;
import ir.alzahra.offerBaz.model.springData.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @Author: hanieh Moafi
 * @Date: 1/18/2020
 **/
@Service
public class UnitServiceImpl implements UnitService {

    @Autowired
    private UnitRepository unitRepository;
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private FoodRepository foodRepository;
    @Autowired
    private ToolsRepository toolsRepository;
    @Autowired
    private MailRepository mailRepository;
    @Autowired
    private GoodsRepository goodsRepository;
    @Autowired
    private EnterExitRepository enterExitRepository;
    @Autowired
    private BuyGoodsRepository buyGoodsRepository;
    @Autowired
    private GoodsConsumptionRepository goodsConsumptionRepository;
    @Autowired
    private EmployeeFoodRepository employeeFoodRepository;

    @Override
    public void insertUnit(UnitEntity unitEntity) throws BaseException {
        if (Objects.isNull(unitEntity.getName()) || Objects.equals(unitEntity.getName(), "")) {
            throw new BaseException("نام سازمان نمیتواند خالی باشد");
        }
        if (Objects.isNull(unitEntity.getUnitNumber())) {
            throw new BaseException("کد سازمان نمیتواند خالی باشد");
        }
        duplicateUnit(unitEntity);
        try {
            unitRepository.save(unitEntity);
        } catch (Exception e) {
            e.printStackTrace();
            throw new BaseException(e.getMessage());
        }
    }

    private void duplicateUnit(UnitEntity u) throws BaseException {
        if (Objects.isNull(u.getId())) {//first submit
            boolean isExist = unitRepository.existsByName(u.getName());
            if (isExist)
                throw new BaseException("نام سازمان نباید تکراری باشد");
        } else {//edit
            List<UnitEntity> ue = unitRepository.findByName(u.getName());
            if (ue.size() > 1) {
                throw new BaseException("نام سازمان نباید تکراری باشد");
            }
        }
    }

    private void duplicateEmployee(EmployeeEntity e) throws BaseException {
        if (Objects.isNull(e.getId())) {//first submit
            List<EmployeeEntity> isExist = employeeRepository.findByPersonalCode(e.getPersonalCode());
            if (Objects.nonNull(isExist) && isExist.size() != 0)
                throw new BaseException("کد پرسنلی نباید تکراری باشد");
        } else {//edit
            List<EmployeeEntity> ue = employeeRepository.findByPersonalCode(e.getPersonalCode());
            if (ue.size() > 1) {
                throw new BaseException("کد پرسنلی نباید تکراری باشد");
            }
        }
    }

    @Override
    public UnitEntity searchUnit(Long code) throws BaseException {
        if (Objects.isNull(code)) {
            throw new BaseException("کد سازمان باید دارای مقدار باشد");
        }
        try {
            UnitEntity unitEntity = new UnitEntity();
            unitEntity = unitRepository.findByUnitNumber(code);
            if (Objects.nonNull(unitEntity)) {
                return unitEntity;
            } else {
                throw new BaseException("سازمانی با کد وارد شده ثبت نشده است");
            }
        } catch (Throwable t) {
            t.printStackTrace();
            throw t;
        }

    }

    @Override
    public void editUnit(UnitEntity unitEntity) throws BaseException {

            if (Objects.nonNull(unitEntity)) {
                if (Objects.isNull(unitEntity.getName()) || Objects.equals(unitEntity.getName(), "")) {
                    throw new BaseException("نام سازمان نمیتواند خالی باشد");
                }
                if (Objects.isNull(unitEntity.getUnitNumber())) {
                    throw new BaseException("کد سازمان نمیتواند خالی باشد");
                }
                try {
                    unitRepository.save(unitEntity);
                } catch (Exception e) {
                    // e.printStackTrace();
                    throw new BaseException(e.getMessage());
                }
            }
    }

    @Override
    public void deleteUnit(UnitEntity unitEntity) throws BaseException {
        try {
            unitRepository.delete(unitEntity);
        } catch (Exception e) {
            // e.printStackTrace();
            throw new BaseException(e.getMessage());
        }
    }

    @Override
    public void insertEmployee(EmployeeEntity employee1) throws BaseException {
        if (Objects.isNull(employee1.getPersonalCode())) {
            throw new BaseException("کد پرسنلی نمیتواند خالی باشد");
        }
        if (Objects.isNull(employee1.getUniName()) || Objects.equals(employee1.getUniName(), "")) {
            throw new BaseException("نام سازمان نمیتواند خالی باشد");
        }
        duplicateEmployee(employee1);
        try {
            employeeRepository.save(employee1);
        } catch (Throwable t) {
            t.printStackTrace();
            throw new BaseException(t.getMessage());

        }
    }

    @Override
    public EmployeeEntity searchEmployee(String personalCode) throws BaseException {
        Long perCode = Long.parseLong(personalCode);
        List<EmployeeEntity> e = employeeRepository.findByPersonalCode(perCode);
        if (Objects.nonNull(e))
            return e.get(0);
        else
            throw new BaseException("کارمند با این کد پرسنلی یافت نشد");
    }

    @Override
    public void deleteEmployee(EmployeeEntity employee3) throws BaseException {

        try {
            employeeRepository.delete(employee3);
        } catch (Exception e) {
            // e.printStackTrace();
            throw new BaseException(e.getMessage());
        }

    }

    @Override
    public void EditEmployee(EmployeeEntity employee3) throws BaseException {
        if (Objects.isNull(employee3.getPersonalCode())) {
            throw new BaseException("کد پرسنلی نمیتواند خالی باشد");
        }
        if (Objects.isNull(employee3.getUniName()) || Objects.equals(employee3.getUniName(), "")) {
            throw new BaseException("نام سازمان نمیتواند خالی باشد");
        }
        duplicateEmployee(employee3);
        try {
            employeeRepository.save(employee3);
        } catch (Exception e) {
            //e.printStackTrace();
            throw new BaseException(e.getMessage());
        }

    }

    @Override
    public List<UnitEntity> getALLUnit() throws BaseException {
        List<UnitEntity> us = unitRepository.findAll();
        if (Objects.nonNull(us))
            return us;
        else throw new BaseException("هیچ سازمانی ثبت نشده است");
    }

    @Override
    public void insertFood(FoodEntity foodEntity) throws BaseException {
        if (Objects.isNull(foodEntity.getDate())) {
            throw new BaseException("تاریخ نمیتواند خالی باشد");
        }
        if (Objects.isNull(foodEntity.getMeal()) || Objects.equals(foodEntity.getMeal(), "")) {
            throw new BaseException("وعده غذایی نمیتواند خالی باشد");
        }
        duplicateFood(foodEntity);
        try {
            foodRepository.save(foodEntity);
        } catch (Throwable t) {
//            t.printStackTrace();
            throw new BaseException(t.getMessage());
        }
    }

    private void duplicateFood(FoodEntity e) throws BaseException {
        if (Objects.isNull(e.getId())) {//first submit
            FoodEntity isExist = foodRepository.findByDateAndMeal(e.getDate(), e.getMeal());
            if (Objects.nonNull(isExist))
                throw new BaseException("کد پرسنلی نباید تکراری باشد");
        }

    }

    @Override
    public FoodEntity searchFood(Date date, String meal) throws BaseException {
        FoodEntity fe = foodRepository.findByDateAndMeal(date, meal);
        if (Objects.nonNull(fe))
            return fe;
        else throw new BaseException("چنین غذایی موجود نیست");
    }

    @Override
    public FoodEntity searchFoodByDateAndMeal(Date date, String meal) throws BaseException {
        try {
            FoodEntity foodEntity = new FoodEntity();
            foodEntity = foodRepository.findByDateAndMeal(date, meal);
            if (Objects.nonNull(foodEntity))
                return foodEntity;
            else throw new BaseException("غذای درخواستی با توجه به تاریخ وارد شده و وعده ی روزانه ثبت نشده است");

        } catch (Throwable t) {
            throw new BaseException(t.getMessage());
        }
    }

    @Override
    public void deleteFood(FoodEntity foodEntity) throws BaseException {
        try {
            foodRepository.delete(foodEntity);
        } catch (Throwable t) {
            // t.printStackTrace();
            throw new BaseException(t.getMessage());

        }
    }

    @Override
    public void EditFood(FoodEntity foodEntity) throws BaseException {
        if (Objects.isNull(foodEntity.getDate())) {
            throw new BaseException("تاریخ نمیتواند خالی باشد");
        }
        if (Objects.isNull(foodEntity.getMeal()) || Objects.equals(foodEntity.getMeal(), "")) {
            throw new BaseException("وعده غذایی نمیتواند خالی باشد");
        }
        duplicateFood(foodEntity);
        try {
            foodRepository.save(foodEntity);
        } catch (Throwable t) {
            //t.printStackTrace();
            throw new BaseException(t.getMessage());
        }
    }

    @Override
    public void insertTools(ToolsEntity toolsEntity) throws BaseException {
        if (Objects.isNull(toolsEntity.getBarcode())) {
            throw new BaseException("بارکد نمیتواند خالی باشد");
        }
        if (Objects.isNull(toolsEntity.getUniName()) || Objects.equals(toolsEntity.getUniName(), "")) {
            throw new BaseException("نام سازمان نمیتواند خالی باشد");
        }
        duplicateTool(toolsEntity);
        try {
            toolsRepository.save(toolsEntity);

        } catch (Throwable t) {
            // t.printStackTrace();
            throw new BaseException(t.getMessage());
        }
    }

    private void duplicateTool(ToolsEntity t) throws BaseException {
        if (Objects.isNull(t.getId())) {//first submit
            ToolsEntity te = toolsRepository.findByBarcode(t.getBarcode());
            if (Objects.nonNull(te))
                throw new BaseException("بارکد نباید تکراری باشد");
        }
    }

    @Override
    public ToolsEntity findToolByBarcode(Long barcode1) throws BaseException {
        ToolsEntity te = toolsRepository.findByBarcode(barcode1);
        if (Objects.nonNull(te))
            return te;
        else throw new BaseException("ابزاری با این بارکد یافت نشد");
    }

    @Override
    public void editTools(ToolsEntity toolsForEdit) throws BaseException {
        if (Objects.isNull(toolsForEdit.getBarcode())) {
            throw new BaseException("بارکد نمیتواند خالی باشد");
        }
        if (Objects.isNull(toolsForEdit.getUniName()) || Objects.equals(toolsForEdit.getUniName(), "")) {
            throw new BaseException("نام سازمان نمیتواند خالی باشد");
        }
        duplicateTool(toolsForEdit);
        try {
            toolsRepository.save(toolsForEdit);

        } catch (Throwable t) {
            //  t.printStackTrace();
            throw new BaseException(t.getMessage());
        }
    }

    @Override
    public void deleteTool(ToolsEntity toolsForEdit) throws BaseException {
        try {
            toolsRepository.delete(toolsForEdit);

        } catch (Throwable t) {
            // t.printStackTrace();
            throw new BaseException(t.getMessage());

        }
    }

    @Override
    public void sendMail(MailEntity mailEntity) throws BaseException {
        if (Objects.isNull(mailEntity.getMailSerial())) {
            throw new BaseException("سریال نمیتواند خالی باشد");
        }
        if (Objects.isNull(mailEntity.getReceiveUnitName()) || Objects.equals(mailEntity.getReceiveUnitName(), "")) {
            throw new BaseException("نام سازمان دریافت کننده نمیتواند خالی باشد");
        }
        if (Objects.isNull(mailEntity.getSendUnitName()) || Objects.equals(mailEntity.getSendUnitName(), "")) {
            throw new BaseException("نام سازمان ارسال کننده نمیتواند خالی باشد");
        }
        Calendar calobj = Calendar.getInstance();
        mailEntity.setDate(calobj.getTime());
        duplicateMail(mailEntity);
        try {
            mailRepository.save(mailEntity);
        } catch (Throwable t) {
            t.printStackTrace();
            throw t;
        }

    }

    private void duplicateMail(MailEntity m) throws BaseException {
        if (Objects.isNull(m.getId())) {//first submit
            MailEntity me = mailRepository.findByMailSerial(m.getMailSerial());
            if (Objects.nonNull(me))
                throw new BaseException("سرریال پیام نباید تکراری باشد");
        }
    }


    @Override
    public MailEntity findMailBySerial(Long serialMail) throws BaseException {

        MailEntity me = mailRepository.findByMailSerial(serialMail);

        if (Objects.nonNull(me))
            return me;
        else throw new BaseException("پیغامی با این سریال یافت نشد");
    }

    @Override
    public void deleteMail(MailEntity mailEntity3) throws BaseException {
        try {
            mailRepository.delete(mailEntity3);

        } catch (Throwable t) {
            //  t.printStackTrace();
            throw new BaseException(t.getMessage());

        }
    }

    @Override
    public void insertGoods(GoodsEntity goodsEntity) throws BaseException {
        if (Objects.isNull(goodsEntity.getUnitName()) || Objects.equals(goodsEntity.getUnitName(), "")) {
            throw new BaseException("نام سازمان نمیتواند خالی باشد");
        }
        if (Objects.isNull(goodsEntity.getBarcode())) {
            throw new BaseException(" بارکد نمیتواند خالی باشد");
        }
        duplicateGoods(goodsEntity);

        try {
            goodsRepository.save(goodsEntity);

        } catch (Throwable t) {
            // t.printStackTrace();
            throw new BaseException(t.getMessage());

        }
    }

    private void duplicateGoods(GoodsEntity g)throws BaseException {
        if (Objects.isNull(g.getId())) {//first submit
            GoodsEntity te = goodsRepository.findByBarcode(g.getBarcode());
            if (Objects.nonNull(te))
                throw new BaseException("بارکد نباید تکراری باشد");
        }
    }


    @Override
    public GoodsEntity searchGoodByBarcode(Long barcode1) throws BaseException {
        GoodsEntity ge = goodsRepository.findByBarcode(barcode1);
        if (Objects.nonNull(ge))
            return ge;
        else throw new BaseException("دارایی با این بارکد یافت نشد");
    }

    @Override
    public void deleteGood(GoodsEntity goodsEntity3) throws BaseException {
        try {
            goodsRepository.delete(goodsEntity3);

        } catch (Throwable t) {
            //t.printStackTrace();
            throw new BaseException(t.getMessage());

        }
    }

    @Override
    public void editGood(GoodsEntity goodsEntity3) throws BaseException {
        if (Objects.isNull(goodsEntity3.getUnitName()) || Objects.equals(goodsEntity3.getUnitName(), "")) {
            throw new BaseException("نام سازمان نمیتواند خالی باشد");
        }
        if (Objects.isNull(goodsEntity3.getBarcode())) {
            throw new BaseException(" بارکد نمیتواند خالی باشد");
        }
        duplicateGoods(goodsEntity3);
        try {
            goodsRepository.save(goodsEntity3);

        } catch (Throwable t) {
            //t.printStackTrace();
            throw new BaseException(t.getMessage());

        }
    }

    @Override
    public void insertEnterExit(EnterExitEntity enterExitEntity) throws BaseException {
        if (Objects.isNull(enterExitEntity.getEnterTime()) || Objects.equals(enterExitEntity.getEnterTime(), "")) {
            throw new BaseException("زمان ورود نمیتواند خالی باشد");
        }
        if (Objects.isNull(enterExitEntity.getExitTime()) || Objects.equals(enterExitEntity.getExitTime(), "")) {

            throw new BaseException("زمان خروج نمیتواند خالی باشد");
        }
        if (Objects.isNull(enterExitEntity.getPersonalCode())) {

            throw new BaseException("کد پرسنلی نمیتواند خالی باشد");
        }
        if (Objects.isNull(enterExitEntity.getUniName()) || Objects.equals(enterExitEntity.getUniName(), "")) {

            throw new BaseException("نام سازمان نمیتواند خالی باشد");
        }
        duplicatEntExi(enterExitEntity);

        try {
            enterExitRepository.save(enterExitEntity);
        } catch (Throwable t) {
            // t.printStackTrace();
            throw new BaseException(t.getMessage());

        }
    }

    private void duplicatEntExi(EnterExitEntity e) {
        //TODO
    }


    @Override
    public EnterExitEntity searchEnterExit(Date date, Long personalCode) throws BaseException {
        try {
            EnterExitEntity enterExitEntities = new EnterExitEntity();
            enterExitEntities = enterExitRepository.findByDateAndPersonalCode(date, personalCode);
            if (Objects.nonNull(enterExitEntities)) {
                return enterExitEntities;
            } else
                throw new BaseException("ورود و خروجی در این تاریخ برای این کد پرسنلی ثبت نشده است");

        } catch (Throwable t) {
            //t.printStackTrace();
            throw new BaseException(t.getMessage());

        }
    }

    @Override
    public void deleteEnterExit(EnterExitEntity enterExitEntity) throws BaseException {
        try {
            enterExitRepository.delete(enterExitEntity);
        } catch (Throwable t) {
            // t.printStackTrace();
            throw new BaseException(t.getMessage());

        }
    }

    @Override
    public void editEnterExit(EnterExitEntity enterExitEntity) throws BaseException {
        if (Objects.isNull(enterExitEntity.getEnterTime())) {
            throw new BaseException("زمان ورود نمیتواند خالی باشد");
        }
        if (Objects.isNull(enterExitEntity.getExitTime())) {

            throw new BaseException("زمان خروج نمیتواند خالی باشد");
        }
        if (Objects.isNull(enterExitEntity.getPersonalCode())) {

            throw new BaseException("کد پرسنلی نمیتواند خالی باشد");
        }
        if (Objects.isNull(enterExitEntity.getUniName()) || Objects.equals(enterExitEntity.getUniName(), "")) {

            throw new BaseException("نام سازمان نمیتواند خالی باشد");
        }
        try {
            enterExitRepository.save(enterExitEntity);
        } catch (Throwable t) {
            // t.printStackTrace();
            throw new BaseException(t.getMessage());

        }
    }

    @Override
    public List<GoodsEntity> findGoodOfUnit(String nameUnit) throws BaseException {
        List<GoodsEntity> ge = goodsRepository.findByUnitName(nameUnit);


        if (Objects.nonNull(ge)) {
            return ge;
        } else throw new BaseException("برای این سازمان دارایی ثبت نشده");

    }

    @Override
    public void insertBuyGoods(BuyGoodsEntity buyGoodsEntity) throws BaseException {
        if (Objects.isNull(buyGoodsEntity.getDate())) {
            throw new BaseException("زمان نمیتواند خالی باشد");
        }
        if (Objects.isNull(buyGoodsEntity.getNameUnit()) || Objects.equals(buyGoodsEntity.getNameUnit(), "")) {

            throw new BaseException("نام سازمان نمیتواند خالی باشد");
        }
        if (Objects.isNull(buyGoodsEntity.getBarcodeGood())) {

            throw new BaseException("دارایی نمیتواند خالی باشد");
        }
        duplicateBuyGood(buyGoodsEntity);
        try {
            buyGoodsRepository.save(buyGoodsEntity);
        } catch (Throwable t) {
            throw new BaseException(t.getMessage());

        }
    }

    private void duplicateBuyGood(BuyGoodsEntity b)throws BaseException {
        if (Objects.isNull(b.getId())) {//first submit
            BuyGoodsEntity te = buyGoodsRepository.findByDate(b.getDate());
            if (Objects.nonNull(te))
                throw new BaseException("دراین تاریخ قبلا خرید ثبت شده است");
        }
    }

    @Override
    public void insertGoodConsumption(GoodsConsumptionEntity goodsConsumptionEntity) throws BaseException {
        if (Objects.isNull(goodsConsumptionEntity.getDate())) {
            throw new BaseException("زمان نمیتواند خالی باشد");
        }
        if (Objects.isNull(goodsConsumptionEntity.getNameUnit()) || Objects.equals(goodsConsumptionEntity.getNameUnit(), "")) {

            throw new BaseException("نام سازمان نمیتواند خالی باشد");
        }
        if (Objects.isNull(goodsConsumptionEntity.getBarcodeGood())) {

            throw new BaseException("دارایی نمیتواند خالی باشد");
        }
        duplicateConsumptionGood(goodsConsumptionEntity);
        try {
            goodsConsumptionRepository.save(goodsConsumptionEntity);
        } catch (Throwable t) {
            throw new BaseException(t.getMessage());

        }
    }
    private void duplicateConsumptionGood(GoodsConsumptionEntity g)throws BaseException {
        if (Objects.isNull(g.getId())) {//first submit
            GoodsConsumptionEntity te = goodsConsumptionRepository.findByDate(g.getDate());
            if (Objects.nonNull(te))
                throw new BaseException("دراین تاریخ قبلا فروش ثبت شده است");
        }
    }

    @Override
    public void insertEmployeeFood(EmployeeFoodEntity employeeFoodEntity) throws BaseException {
        if (Objects.isNull(employeeFoodEntity.getPersonalCode()) || Objects.equals(employeeFoodEntity.getPersonalCode(), "")) {
            throw new BaseException("کد پرسنلی نمیتواند خالی باشد");
        }
        if (Objects.isNull(employeeFoodEntity.getUniName()) || Objects.equals(employeeFoodEntity.getUniName(), "")) {

            throw new BaseException("نام سازمان نمیتواند خالی باشد");
        }
        if (Objects.isNull(employeeFoodEntity.getDate())) {

            throw new BaseException("تاریخ نمیتواند خالی باشد");
        }
        if (Objects.isNull(employeeFoodEntity.getMeal()) || Objects.equals(employeeFoodEntity.getMeal(), "")) {

            throw new BaseException("وعده غذایی نمیتواند خالی باشد");
        }
        try {
            employeeFoodRepository.save(employeeFoodEntity);
        } catch (Throwable t) {
            throw new BaseException(t.getMessage());

        }
    }

    @Override
    public EmployeeFoodEntity searchEmployeeFood(Date date, String meal, Long code) throws BaseException {
        try {
            EmployeeFoodEntity employeeFoodEntity = new EmployeeFoodEntity();
            employeeFoodEntity = employeeFoodRepository.findByDateAndMealAndPersonalCode(date, meal, code);
            if (Objects.nonNull(employeeFoodEntity))
                return employeeFoodEntity;
            else
                throw new BaseException("با توجه به اطلاعات وارد شده غذایی برای کارمند ثبت نشده است.");
        } catch (Throwable t) {
            throw new BaseException(t.getMessage());

        }
    }

    @Override
    public void deleteEmployeeFood(EmployeeFoodEntity employeeFoodEntity) throws BaseException {
        try {
            if (Objects.nonNull(employeeFoodEntity))
                employeeFoodRepository.delete(employeeFoodEntity);
        } catch (Throwable e) {
            throw new BaseException(e.getMessage());

        }
    }
}
