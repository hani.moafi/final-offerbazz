package ir.alzahra.offerBaz.view.beans;

import ir.alzahra.offerBaz.control.UnitService;
import ir.alzahra.offerBaz.exception.BaseException;
import ir.alzahra.offerBaz.model.entity.EmployeeEntity;
import ir.alzahra.offerBaz.model.entity.UnitEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @Author: hanieh Moafi
 * @Date: 1/18/2020
 **/
@Component("employeeBean")
@Scope("view")
public class EmployeeBean {


    private EmployeeEntity employee1;
    private EmployeeEntity employee2;
    private EmployeeEntity employee3;
    private String personalCode;
    private String personalCodeforEdit;
    private List<UnitEntity> unitEntities;
    private List<String> unitsName;

    @Autowired
    private UnitService unitService;


    public void init() {
        unitEntities = new ArrayList<>();
        unitsName = new ArrayList<>();
        emptyPage();
        try {
            unitEntities = unitService.getALLUnit();
            for (UnitEntity u : unitEntities
                    ) {
                unitsName.add(u.getName());
            }
        } catch (BaseException e) {
//            e.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));

        }
    }

    public void emptyPage() {
        employee1 = new EmployeeEntity();
        employee2 = new EmployeeEntity();
        employee3 = new EmployeeEntity();
        personalCode = null;
        personalCodeforEdit = null;


    }

    public EmployeeEntity getEmployee1() {
        return employee1;
    }

    public void setEmployee1(EmployeeEntity employee1) {
        this.employee1 = employee1;
    }

    public String getPersonalCode() {
        return personalCode;
    }

    public void setPersonalCode(String personalCode) {
        this.personalCode = personalCode;
    }

    public EmployeeEntity getEmployee2() {
        return employee2;
    }

    public void setEmployee2(EmployeeEntity employee2) {
        this.employee2 = employee2;
    }

    public String getPersonalCodeforEdit() {
        return personalCodeforEdit;
    }

    public void setPersonalCodeforEdit(String personalCodeforEdit) {
        this.personalCodeforEdit = personalCodeforEdit;
    }

    public EmployeeEntity getEmployee3() {
        return employee3;
    }

    public void setEmployee3(EmployeeEntity employee3) {
        this.employee3 = employee3;
    }

    public List<UnitEntity> getUnitEntities() {
        return unitEntities;
    }

    public void setUnitEntities(List<UnitEntity> unitEntities) {
        this.unitEntities = unitEntities;
    }

    public List<String> getUnitsName() {
        return unitsName;
    }

    public void setUnitsName(List<String> unitsName) {
        this.unitsName = unitsName;
    }

    public void insertEmployee() {
        if (Objects.nonNull(employee1))
            try {
                unitService.insertEmployee(employee1);
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "کارمند با موفقیت ثبت گردید", ""));

                emptyPage();
            } catch (BaseException e) {
//                e.printStackTrace();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));

            }

    }

    public void searchEmployee() {
        if (Objects.nonNull(personalCode) && !Objects.equals(personalCode, ""))
            try {
                employee2 = unitService.searchEmployee(personalCode);
            } catch (BaseException e) {
//                e.printStackTrace();
                emptyPage();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));

            }

    }

    public void searchEmployeeForEdit() {
        if (Objects.nonNull(personalCodeforEdit) && !Objects.equals(personalCodeforEdit, ""))
            try {
                employee3 = unitService.searchEmployee(personalCodeforEdit);
            } catch (BaseException e) {
//                e.printStackTrace();
                emptyPage();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));

            }

    }

    public void deleteEmployee() {
        if (Objects.nonNull(employee3) && Objects.nonNull(employee3.getId()))
            try {
                unitService.deleteEmployee(employee3);
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "کارمند با موفقیت حذف گردید", ""));

                emptyPage();
            } catch (BaseException e) {
//               e.printStackTrace();
                emptyPage();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));

            }


    }

    public void editEmployee() {
        try {
            unitService.EditEmployee(employee3);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "کارمند با موفقیت ویرایش گردید", ""));
            emptyPage();
        } catch (BaseException e) {
//            e.printStackTrace();
            emptyPage();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));

        }

    }
}
