package ir.alzahra.offerBaz.view.beans;


import ir.alzahra.offerBaz.control.UnitService;
import ir.alzahra.offerBaz.exception.BaseException;
import ir.alzahra.offerBaz.model.entity.ToolsEntity;
import ir.alzahra.offerBaz.model.entity.UnitEntity;
import org.hibernate.envers.Audited;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @Author: hanieh Moafi
 * @Date: 1/19/2020
 **/
@Component("toolsBean")
@Scope("view")
public class ToolsBean {

    private ToolsEntity toolsEntity;
    private ToolsEntity toolsForView;
    private ToolsEntity toolsForEdit;

    private List<UnitEntity> unitEntities = new ArrayList<>();
    private List<String> unitsName = new ArrayList<>();
    private Long barcode1;
    private Long barcode2;
    @Autowired
    private UnitService unitService;


    public void init() {
        try {
            unitEntities = unitService.getALLUnit();
            if (Objects.nonNull(unitEntities)) {
                for (UnitEntity u : unitEntities
                        ) {
                    unitsName.add(u.getName());
                }
            }
        } catch (BaseException e) {
           // e.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,  e.getMessage(),""));

        }
        if (Objects.isNull(toolsEntity))
            emptyPage();
    }

    private void emptyPage() {
        toolsEntity = new ToolsEntity();
        toolsForView = new ToolsEntity();
        toolsForEdit=new ToolsEntity();
    }

    public ToolsEntity getToolsEntity() {
        return toolsEntity;
    }

    public void setToolsEntity(ToolsEntity toolsEntity) {
        this.toolsEntity = toolsEntity;
    }

    public List<UnitEntity> getUnitEntities() {
        return unitEntities;
    }

    public void setUnitEntities(List<UnitEntity> unitEntities) {
        this.unitEntities = unitEntities;
    }

    public List<String> getUnitsName() {
        return unitsName;
    }

    public void setUnitsName(List<String> unitsName) {
        this.unitsName = unitsName;
    }

    public Long getBarcode1() {
        return barcode1;
    }

    public void setBarcode1(Long barcode1) {
        this.barcode1 = barcode1;
    }

    public Long getBarcode2() {
        return barcode2;
    }

    public void setBarcode2(Long barcode2) {
        this.barcode2 = barcode2;
    }

    public void editTools() {
        try {
            unitService.editTools(toolsForEdit);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "ابزار با موفقیت ویرایش شد", ""));

        } catch (BaseException e) {
//            e.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,  e.getMessage(),""));

        }

    }

    public void deleteTools() {
        try {
            unitService.deleteTool(toolsForEdit);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "ابزار با موفقیت حذف گردید", ""));

            emptyPage();
        } catch (BaseException e) {
           // e.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,  e.getMessage(),""));

        }
    }

    public ToolsEntity getToolsForView() {
        return toolsForView;
    }

    public void setToolsForView(ToolsEntity toolsForView) {
        this.toolsForView = toolsForView;
    }

    public ToolsEntity getToolsForEdit() {
        return toolsForEdit;
    }

    public void setToolsForEdit(ToolsEntity toolsForEdit) {
        this.toolsForEdit = toolsForEdit;
    }

    public void insertTools() {

        try {
            for (UnitEntity u:unitEntities
                 ) {
                if (Objects.equals(u.getName(),toolsEntity.getUniName())){
                    toolsEntity.setUnitEntity(u);
                    break;
                }
            }
            unitService.insertTools(toolsEntity);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "ابزار با موفقیت ثبت گردید", ""));

            emptyPage();
        } catch (BaseException e) {
//            e.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,  e.getMessage(),""));

        }

    }

    public void searchToolForView() {
        try {
            toolsForView = unitService.findToolByBarcode(barcode1);
        } catch (BaseException e) {
            //e.printStackTrace();
            emptyPage();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,  e.getMessage(),""));

        }
    }

    public void searchToolForEdit() {
        try {
            toolsForEdit = unitService.findToolByBarcode(barcode2);
            barcode2=null;
        } catch (BaseException e) {
           // e.printStackTrace();
            emptyPage();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,  e.getMessage(),""));

        }
    }
}
