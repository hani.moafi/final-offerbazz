package ir.alzahra.offerBaz.view.beans;

import ir.alzahra.offerBaz.control.UnitService;
import ir.alzahra.offerBaz.exception.BaseException;
import ir.alzahra.offerBaz.model.entity.MailEntity;
import ir.alzahra.offerBaz.model.entity.UnitEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @Author : Moafi
 * @DATE :19/01/2020
 **/

@Component("sendMail")
@Scope("view")
public class SendMail {

    private MailEntity mailEntity;
    private MailEntity mailEntity2;
    private MailEntity mailEntity3;
    private List<UnitEntity> unitEntities = new ArrayList<>();
    private List<String> unitsName = new ArrayList<>();
    private Long serialMail;
    private Long serialMail2;
    @Autowired
    private UnitService unitService;


    public void init() {
        try {
            unitEntities = unitService.getALLUnit();
            for (UnitEntity u : unitEntities
                    ) {
                unitsName.add(u.getName());
            }
        } catch (BaseException e) {
            // e.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));

        }
        emptyPage();
    }

    private void emptyPage() {
        if (Objects.isNull(mailEntity))
            mailEntity = new MailEntity();
        mailEntity2 = new MailEntity();
        mailEntity3 = new MailEntity();
    }

    public MailEntity getMailEntity() {
        return mailEntity;
    }

    public void setMailEntity(MailEntity mailEntity) {
        this.mailEntity = mailEntity;
    }

    public List<UnitEntity> getUnitEntities() {
        return unitEntities;
    }

    public void setUnitEntities(List<UnitEntity> unitEntities) {
        this.unitEntities = unitEntities;
    }

    public List<String> getUnitsName() {
        return unitsName;
    }

    public void setUnitsName(List<String> unitsName) {
        this.unitsName = unitsName;
    }

    public Long getSerialMail() {
        return serialMail;
    }

    public void setSerialMail(Long serialMail) {
        this.serialMail = serialMail;
    }

    public MailEntity getMailEntity2() {
        return mailEntity2;
    }

    public void setMailEntity2(MailEntity mailEntity2) {
        this.mailEntity2 = mailEntity2;
    }

    public Long getSerialMail2() {
        return serialMail2;
    }

    public void setSerialMail2(Long serialMail2) {
        this.serialMail2 = serialMail2;
    }

    public MailEntity getMailEntity3() {
        return mailEntity3;
    }

    public void setMailEntity3(MailEntity mailEntity3) {
        this.mailEntity3 = mailEntity3;
    }

    public void sendMail() {
        for (UnitEntity u : unitEntities
                ) {
            if (Objects.equals(u.getName(), mailEntity.getReceiveUnitName())) {
                mailEntity.setRecievUnitEntity(u);
            }
            if (Objects.equals(u.getName(), mailEntity.getSendUnitName())) {
                mailEntity.setSendUnitEntity(u);
            }
        }

        try {
            unitService.sendMail(mailEntity);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "پیام با موفقیت ثبت گردید", ""));
            emptyPage();
        } catch (BaseException e) {
//            e.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));

        }
    }

    public void searchMail() {
        try {
            mailEntity2 = unitService.findMailBySerial(serialMail);
        } catch (BaseException e) {
            // e.printStackTrace();
            emptyPage();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));

        }
    }

    public void searchMailForDelete() {
        try {
            mailEntity3 = unitService.findMailBySerial(serialMail2);
        } catch (BaseException e) {
            //  e.printStackTrace();
            emptyPage();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));

        }
    }

    public void deleteMail() {
        try {
            unitService.deleteMail(mailEntity3);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "پیغام با موفقیت حذف گردید", ""));
            serialMail2 = null;
            emptyPage();
        } catch (BaseException e) {
            // e.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));

        }
    }
}
