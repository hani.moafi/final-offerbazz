package ir.alzahra.offerBaz.view.beans;

import ir.alzahra.offerBaz.control.UnitService;
import ir.alzahra.offerBaz.exception.BaseException;
import ir.alzahra.offerBaz.model.entity.EnterExitEntity;
import ir.alzahra.offerBaz.model.entity.UnitEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.util.ArrayList;

/**
 * @Author: hanieh Moafi
 * @Date: 1/17/2020
 **/
@Component("unitBean")
@Scope("view")
public class UnitBean {

    @Autowired
    private UnitService unitService;

    private UnitEntity unitEntity;
    private UnitEntity searchUnitEntity;
    private UnitEntity editUnitEntity;

    private Long searchUnitCode;
    private Long editUnitCode;


    public void init() {
        emptyPage();
    }

    public void emptyPage() {
        unitEntity = new UnitEntity();
        searchUnitEntity = new UnitEntity();
        editUnitEntity = new UnitEntity();
    }

    public void insert() {
        try {
            unitService.insertUnit(unitEntity);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "سازمان ثبت شد", ""));
            emptyPage();
        } catch (BaseException e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));
            // e.printStackTrace();
        }
    }

    public void searchUnit() {
        try {
            searchUnitEntity = unitService.searchUnit(searchUnitCode);
        } catch (BaseException e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));
        }
    }

    public void searchUnitForEdit() {
        try {
            editUnitEntity = unitService.searchUnit(editUnitCode);
        } catch (BaseException e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));
        }
    }

    public void editUnit() {
        try {
            unitService.editUnit(editUnitEntity);
            emptyPage();
            editUnitCode=null;
            searchUnitCode=null;
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "سازمان ویرایش شد", ""));
        } catch (BaseException e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));
        }
    }

    public void deleteUnit() {
        try {
            unitService.deleteUnit(editUnitEntity);
            emptyPage();
            editUnitCode=null;
            searchUnitCode=null;
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "سازمان حذف شد", ""));
        } catch (BaseException e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));
        }
    }


    public UnitEntity getUnitEntity() {
        return unitEntity;
    }

    public void setUnitEntity(UnitEntity unitEntity) {
        this.unitEntity = unitEntity;
    }

    public Long getSearchUnitCode() {
        return searchUnitCode;
    }

    public void setSearchUnitCode(Long searchUnitCode) {
        this.searchUnitCode = searchUnitCode;
    }

    public Long getEditUnitCode() {
        return editUnitCode;
    }

    public void setEditUnitCode(Long editUnitCode) {
        this.editUnitCode = editUnitCode;
    }

    public UnitEntity getSearchUnitEntity() {
        return searchUnitEntity;
    }

    public void setSearchUnitEntity(UnitEntity searchUnitEntity) {
        this.searchUnitEntity = searchUnitEntity;
    }

    public UnitEntity getEditUnitEntity() {
        return editUnitEntity;
    }

    public void setEditUnitEntity(UnitEntity editUnitEntity) {
        this.editUnitEntity = editUnitEntity;
    }
}
