package ir.alzahra.offerBaz.view.beans;

import ir.alzahra.offerBaz.control.UnitService;
import ir.alzahra.offerBaz.exception.BaseException;
import ir.alzahra.offerBaz.model.entity.BuyGoodsEntity;
import ir.alzahra.offerBaz.model.entity.GoodsConsumptionEntity;
import ir.alzahra.offerBaz.model.entity.GoodsEntity;
import ir.alzahra.offerBaz.model.entity.UnitEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @Author : Moafi
 * @DATE :20/01/2020
 **/
@Component("goodConsumptionBean")
@Scope("view")
public class GoodConsumptionBean {

    private GoodsConsumptionEntity goodsConsumptionEntity;
    private List<UnitEntity> unitEntities;
    private List<String> unitsName;
    private List<GoodsEntity> goodsEntities;


    @Autowired
    private UnitService unitService;


    public void init() {
        emptyPage();
        try {
            unitEntities = unitService.getALLUnit();
            for (UnitEntity u : unitEntities
                    ) {
                unitsName.add(u.getName());
            }
        } catch (BaseException e) {
            //e.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));

        }


    }

    private void emptyPage() {
        goodsConsumptionEntity = new GoodsConsumptionEntity();
        unitEntities = new ArrayList<>();
        unitsName = new ArrayList<>();
        goodsEntities = new ArrayList<>();


    }

    public GoodsConsumptionEntity getGoodsConsumptionEntity() {
        return goodsConsumptionEntity;
    }

    public void setGoodsConsumptionEntity(GoodsConsumptionEntity goodsConsumptionEntity) {
        this.goodsConsumptionEntity = goodsConsumptionEntity;
    }

    public List<UnitEntity> getUnitEntities() {
        return unitEntities;
    }

    public void setUnitEntities(List<UnitEntity> unitEntities) {
        this.unitEntities = unitEntities;
    }

    public List<String> getUnitsName() {
        return unitsName;
    }

    public void setUnitsName(List<String> unitsName) {
        this.unitsName = unitsName;
    }

    public List<GoodsEntity> getGoodsEntities() {
        return goodsEntities;
    }

    public void setGoodsEntities(List<GoodsEntity> goodsEntities) {
        this.goodsEntities = goodsEntities;
    }

    public void onGoodChange() {
        if (Objects.nonNull(goodsConsumptionEntity.getNameUnit()) && !Objects.equals(goodsConsumptionEntity.getNameUnit(), "")) {
            try {
                goodsEntities = unitService.findGoodOfUnit(goodsConsumptionEntity.getNameUnit());
            } catch (BaseException e) {
                // e.printStackTrace();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));

            }
        }

    }

    public void insert() {
        for (GoodsEntity g : goodsEntities
                ) {
            if (g.getUnitName().equals(goodsConsumptionEntity.getNameUnit()))
                goodsConsumptionEntity.setGoodsEntity(g);
        }
        try {
            unitService.insertGoodConsumption(goodsConsumptionEntity);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "اطلاعات فروش با موفقیت ثبت گردید", ""));
            emptyPage();
        } catch (BaseException e) {
            //  e.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));

        }


    }
}
