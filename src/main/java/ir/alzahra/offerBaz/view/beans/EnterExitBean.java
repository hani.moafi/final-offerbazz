package ir.alzahra.offerBaz.view.beans;

import ir.alzahra.offerBaz.control.UnitService;
import ir.alzahra.offerBaz.exception.BaseException;
import ir.alzahra.offerBaz.model.entity.EnterExitEntity;
import ir.alzahra.offerBaz.model.entity.UnitEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * @Author: sobhan_ssh
 **/

@Component("enterExitBean")
@Scope("view")
public class EnterExitBean {

    @Autowired
    private UnitService unitService;

    private EnterExitEntity insertEnterExitEntity;
    private EnterExitEntity searchEnterExitEntity;
    private EnterExitEntity editEnterExitEntity;
    private List<UnitEntity> unitEntities;
    private List<String> unitsName;
    private Long personalCode;
    private Date date;
    private Long editPersonalCode;
    private Date editDate;

    public void emptyPage() {
        insertEnterExitEntity = new EnterExitEntity();
        searchEnterExitEntity = new EnterExitEntity();
        editEnterExitEntity = new EnterExitEntity();
        unitEntities = new ArrayList<>();
        unitsName = new ArrayList<>();
    }

    public void init() {
        emptyPage();
        try {
            unitEntities = unitService.getALLUnit();
            for (UnitEntity u : unitEntities
                    ) {
                unitsName.add(u.getName());
            }
        } catch (BaseException e) {
//            e.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));

        }
    }

    public void insertEnterExit() {
        if (Objects.nonNull(insertEnterExitEntity)) {
            try {
                unitService.insertEnterExit(insertEnterExitEntity);
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "ورود/خروج با موقیت ثبت گردید", ""));
                emptyPage();
            } catch (BaseException e) {
//                e.printStackTrace();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));

            }
        }
    }

    public void searchEnterExit() {
        if (Objects.nonNull(personalCode) && Objects.nonNull(date)) {
            try {
                searchEnterExitEntity = unitService.searchEnterExit(date,personalCode);
                personalCode = null;
                date=null;
            } catch (BaseException e) {
//                e.printStackTrace();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));
            }
        }
    }

    public void searchEnterExitForEdit() {
        if (Objects.nonNull(editPersonalCode) && Objects.nonNull(editDate)) {
            try {
                editEnterExitEntity = unitService.searchEnterExit(editDate,editPersonalCode);
                editDate= null;
                editPersonalCode=null;
            } catch (BaseException e) {
//                e.printStackTrace();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));

            }
        }
    }

    public void deleteEnterExit() {
        if (Objects.nonNull(editEnterExitEntity))
            try {
                unitService.deleteEnterExit(editEnterExitEntity);
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "ورود/خروج با موفقیت حذف گردید", ""));
                emptyPage();
                editPersonalCode = null;
                editDate= null;
            } catch (BaseException e) {
//                e.printStackTrace();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));

            }


    }

    public void editEnterExit() {
        try {
            unitService.editEnterExit(editEnterExitEntity);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "ورود/خروج با موقیت ویرایش گردید", ""));
            emptyPage();
            editPersonalCode = null;
            editDate= null;
        } catch (BaseException e) {
//            e.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));

        }

    }

    public EnterExitEntity getInsertEnterExitEntity() {
        return insertEnterExitEntity;
    }

    public void setInsertEnterExitEntity(EnterExitEntity insertEnterExitEntity) {
        this.insertEnterExitEntity = insertEnterExitEntity;
    }

    public EnterExitEntity getSearchEnterExitEntity() {
        return searchEnterExitEntity;
    }

    public void setSearchEnterExitEntity(EnterExitEntity searchEnterExitEntity) {
        this.searchEnterExitEntity = searchEnterExitEntity;
    }

    public EnterExitEntity getEditEnterExitEntity() {
        return editEnterExitEntity;
    }

    public void setEditEnterExitEntity(EnterExitEntity editEnterExitEntity) {
        this.editEnterExitEntity = editEnterExitEntity;
    }

    public List<UnitEntity> getUnitEntities() {
        return unitEntities;
    }

    public void setUnitEntities(List<UnitEntity> unitEntities) {
        this.unitEntities = unitEntities;
    }

    public List<String> getUnitsName() {
        return unitsName;
    }

    public void setUnitsName(List<String> unitsName) {
        this.unitsName = unitsName;
    }

    public Long getPersonalCode() {
        return personalCode;
    }

    public void setPersonalCode(Long personalCode) {
        this.personalCode = personalCode;
    }

    public Long getEditPersonalCode() {
        return editPersonalCode;
    }

    public void setEditPersonalCode(Long editPersonalCode) {
        this.editPersonalCode = editPersonalCode;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getEditDate() {
        return editDate;
    }

    public void setEditDate(Date editDate) {
        this.editDate = editDate;
    }
}
