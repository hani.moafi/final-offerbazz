package ir.alzahra.offerBaz.view.beans;

import ir.alzahra.offerBaz.control.UnitService;
import ir.alzahra.offerBaz.exception.BaseException;
import ir.alzahra.offerBaz.model.entity.GoodsEntity;
import ir.alzahra.offerBaz.model.entity.UnitEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @Author : Moafi
 * @DATE :19/01/2020
 **/

@Component("goodsBean")
@Scope("view")
public class GoodsBean {


    private GoodsEntity goodsEntity;
    private GoodsEntity goodsEntity2;
    private GoodsEntity goodsEntity3;
    private Long barcode1;
    private Long barcode2;
    private List<UnitEntity> unitEntities;
    private List<String> unitsName;
    @Autowired
    private UnitService unitService;

    public void init() {
        unitEntities = new ArrayList<>();
        unitsName = new ArrayList<>();
        emptyPage();

        try {
            unitEntities = unitService.getALLUnit();
            for (UnitEntity u : unitEntities
                    ) {
                unitsName.add(u.getName());
            }
        } catch (BaseException e) {
//            e.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));

        }

    }

    private void emptyPage() {
        goodsEntity = new GoodsEntity();
        goodsEntity2 = new GoodsEntity();
        goodsEntity3 = new GoodsEntity();

    }

    public GoodsEntity getGoodsEntity() {
        return goodsEntity;
    }

    public void setGoodsEntity(GoodsEntity goodsEntity) {
        this.goodsEntity = goodsEntity;
    }

    public GoodsEntity getGoodsEntity2() {
        return goodsEntity2;
    }

    public void setGoodsEntity2(GoodsEntity goodsEntity2) {
        this.goodsEntity2 = goodsEntity2;
    }

    public GoodsEntity getGoodsEntity3() {
        return goodsEntity3;
    }

    public void setGoodsEntity3(GoodsEntity goodsEntity3) {
        this.goodsEntity3 = goodsEntity3;
    }

    public Long getBarcode1() {
        return barcode1;
    }

    public void setBarcode1(Long barcode1) {
        this.barcode1 = barcode1;
    }

    public Long getBarcode2() {
        return barcode2;
    }

    public void setBarcode2(Long barcode2) {
        this.barcode2 = barcode2;
    }


    public List<UnitEntity> getUnitEntities() {
        return unitEntities;
    }

    public void setUnitEntities(List<UnitEntity> unitEntities) {
        this.unitEntities = unitEntities;
    }

    public List<String> getUnitsName() {
        return unitsName;
    }

    public void setUnitsName(List<String> unitsName) {
        this.unitsName = unitsName;
    }

    public void insert() {
        try {
            for (UnitEntity u : unitEntities
                    ) {
                if (Objects.equals(goodsEntity.getUnitName(), u.getName())) {
                    goodsEntity.setUnitEntity(u);
                }
            }
            unitService.insertGoods(goodsEntity);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "دارایی با موفقیت ثبت گردید", ""));
            emptyPage();
        } catch (BaseException e) {
//            e.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));

        }

    }

    public void searchGood() {
        try {
            goodsEntity2 = unitService.searchGoodByBarcode(barcode1);
        } catch (BaseException e) {
//            e.printStackTrace();
            emptyPage();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));

        }
    }

    public void searchGoodForEdit() {
        try {
            goodsEntity3 = unitService.searchGoodByBarcode(barcode2);
        } catch (BaseException e) {
//            e.printStackTrace();
            emptyPage();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));

        }
    }

    public void delete() {
        try {
            unitService.deleteGood(goodsEntity3);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "دارایی با موفقیت حذف گردید", ""));
            barcode2 = null;
            emptyPage();
        } catch (BaseException e) {
//            e.printStackTrace();
            emptyPage();
            barcode2 = null;
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));

        }

    }

    public void edit() {

        try {
            unitService.editGood(goodsEntity3);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "دارایی با موفقیت ویرایش گردید", ""));
            barcode2 = null;
            emptyPage();
        } catch (BaseException e) {
//            e.printStackTrace();
            emptyPage();
            barcode2 = null;

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));

        }
    }
}
