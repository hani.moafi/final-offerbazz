package ir.alzahra.offerBaz.view.beans;

import ir.alzahra.offerBaz.control.UnitService;
import ir.alzahra.offerBaz.exception.BaseException;
import ir.alzahra.offerBaz.model.entity.FoodEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.util.Date;
import java.util.Objects;

/**
 * @Author: sobhan_ssh
 **/

@Component("foodBean")
@Scope("view")
public class FoodBean {

    private FoodEntity insertFoodEntity;
    private FoodEntity searchFoodEntity;
    private FoodEntity editFoodEntity;
    private String searchMeal;
    private Date searchDate;
    private String delMeal;
    private Date delDate;

    @Autowired
    private UnitService unitService;

    public void init() {
        emptyPage();
    }

    public void emptyPage() {
        insertFoodEntity = new FoodEntity();
        searchFoodEntity = new FoodEntity();
        editFoodEntity = new FoodEntity();
    }

    public void insert() {
        if (Objects.nonNull(insertFoodEntity)) {
            try {
                unitService.insertFood(insertFoodEntity);
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "غذا با موفقیت ثبت گردید", ""));

                emptyPage();
            } catch (BaseException e) {
//                e.printStackTrace();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,  e.getMessage(),""));

            }
        }
    }

    public void searchFood() {
        if (Objects.nonNull(searchDate) && Objects.nonNull(searchMeal) ) {
            try {
               searchFoodEntity = unitService.searchFood(searchDate,searchMeal);
            } catch (BaseException e){
//                e.printStackTrace();
                emptyPage();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,  e.getMessage(),""));

            }
        }
    }

    public void searchFoodForEdit() {
        if (Objects.nonNull(delDate) && Objects.nonNull(delMeal)) {
            try {
              editFoodEntity = unitService.searchFood(delDate,delMeal);
            } catch (BaseException e){
//                e.printStackTrace();
                emptyPage();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,  e.getMessage(),""));

            }
        }
    }

    public void deleteFood(){
        if (Objects.nonNull(editFoodEntity))
            try {
                unitService.deleteFood(editFoodEntity);
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "غذا با موفقیت حذف گردید", ""));

                emptyPage();
                delDate=null;
                delMeal=null;
            } catch (BaseException e) {
//                e.printStackTrace();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,  e.getMessage(),""));

            }


    }

    public void editFood(){
        try {
            unitService.EditFood(editFoodEntity);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "غذا با موفقیت ویرایش گردید", ""));
            emptyPage();
            delDate=null;
            delMeal=null;
        } catch (BaseException e) {
//            e.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,  e.getMessage(),""));

        }

    }

    public FoodEntity getInsertFoodEntity() {
        return insertFoodEntity;
    }

    public void setInsertFoodEntity(FoodEntity foodEntity) {
        this.insertFoodEntity = foodEntity;
    }

    public FoodEntity getSearchFoodEntity() {
        return searchFoodEntity;
    }

    public void setSearchFoodEntity(FoodEntity searchFoodEntity) {
        this.searchFoodEntity = searchFoodEntity;
    }

    public FoodEntity getEditFoodEntity() {
        return editFoodEntity;
    }

    public void setEditFoodEntity(FoodEntity editFoodEntity) {
        this.editFoodEntity = editFoodEntity;
    }

    public String getSearchMeal() {
        return searchMeal;
    }

    public void setSearchMeal(String searchMeal) {
        this.searchMeal = searchMeal;
    }

    public Date getSearchDate() {
        return searchDate;
    }

    public void setSearchDate(Date searchDate) {
        this.searchDate = searchDate;
    }

    public String getDelMeal() {
        return delMeal;
    }

    public void setDelMeal(String delMeal) {
        this.delMeal = delMeal;
    }

    public Date getDelDate() {
        return delDate;
    }

    public void setDelDate(Date delDate) {
        this.delDate = delDate;
    }
}


