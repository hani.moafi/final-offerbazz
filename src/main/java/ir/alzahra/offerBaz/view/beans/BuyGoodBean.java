package ir.alzahra.offerBaz.view.beans;

import ir.alzahra.offerBaz.control.UnitService;
import ir.alzahra.offerBaz.exception.BaseException;
import ir.alzahra.offerBaz.model.entity.BuyGoodsEntity;
import ir.alzahra.offerBaz.model.entity.GoodsEntity;
import ir.alzahra.offerBaz.model.entity.UnitEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @Author: hanieh Moafi
 * @Date: 1/19/2020
 **/
@Component("buyGoodsBean")
@Scope("view")
public class BuyGoodBean {


    private BuyGoodsEntity buyGoodsEntity;
    private List<UnitEntity> unitEntities;
    private List<String> unitsName;
    private List<GoodsEntity> goodsEntities;

    @Autowired
    private UnitService unitService;


    public void init() {
        emptyPage();

        try {
            unitEntities = unitService.getALLUnit();
            for (UnitEntity u : unitEntities
                    ) {
                unitsName.add(u.getName());
            }
        } catch (BaseException e) {
//            e.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));

        }

    }

    private void emptyPage() {
        unitEntities = new ArrayList<>();
        unitsName = new ArrayList<>();
        buyGoodsEntity = new BuyGoodsEntity();
        goodsEntities = new ArrayList<>();
    }

    public BuyGoodsEntity getBuyGoodsEntity() {
        return buyGoodsEntity;
    }

    public void setBuyGoodsEntity(BuyGoodsEntity buyGoodsEntity) {
        this.buyGoodsEntity = buyGoodsEntity;
    }

    public List<UnitEntity> getUnitEntities() {
        return unitEntities;
    }

    public void setUnitEntities(List<UnitEntity> unitEntities) {
        this.unitEntities = unitEntities;
    }

    public List<String> getUnitsName() {
        return unitsName;
    }

    public void setUnitsName(List<String> unitsName) {
        this.unitsName = unitsName;
    }

    public List<GoodsEntity> getGoodsEntities() {
        return goodsEntities;
    }

    public void setGoodsEntities(List<GoodsEntity> goodsEntities) {
        this.goodsEntities = goodsEntities;
    }

    public void insert() {
        for (GoodsEntity g : goodsEntities
                ) {
            if (g.getUnitName().equals(buyGoodsEntity.getNameUnit()))
                buyGoodsEntity.setGoodsEntity(g);
        }
        try {
            unitService.insertBuyGoods(buyGoodsEntity);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "اطلاعات خرید با موفقیت ثبت گردید", ""));
            emptyPage();
        } catch (BaseException e) {
//            e.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));

        }


    }

    public void onGoodChange() {
        if (Objects.nonNull(buyGoodsEntity.getNameUnit()) && !Objects.equals(buyGoodsEntity.getNameUnit(), "")) {
            try {
                goodsEntities = unitService.findGoodOfUnit(buyGoodsEntity.getNameUnit());
            } catch (BaseException e) {
//                e.printStackTrace();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));

            }
        }

    }
}
