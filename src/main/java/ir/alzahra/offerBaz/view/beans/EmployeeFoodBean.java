package ir.alzahra.offerBaz.view.beans;

import ir.alzahra.offerBaz.control.UnitService;
import ir.alzahra.offerBaz.exception.BaseException;
import ir.alzahra.offerBaz.model.entity.EmployeeEntity;
import ir.alzahra.offerBaz.model.entity.EmployeeFoodEntity;
import ir.alzahra.offerBaz.model.entity.FoodEntity;
import ir.alzahra.offerBaz.model.entity.UnitEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * @Author: sobhan_ssh
 **/

@Component("employeeFood")
@Scope("view")
public class EmployeeFoodBean {

    @Autowired
    private UnitService unitService;

    private EmployeeFoodEntity insertEmployeeFood;
    private EmployeeFoodEntity searchEmployeeFood;
    private EmployeeFoodEntity editEmployeeFood;
    private List<UnitEntity> unitEntities;
    private List<String> unitsName;
    private String searchMeal;
    private Date searchDate;
    private Long personalCode;
    private String delMeal;
    private Date delDate;
    private Long delPersoanalCode;

    public void emptyPage() {
        insertEmployeeFood = new EmployeeFoodEntity();
        searchEmployeeFood = new EmployeeFoodEntity();
        editEmployeeFood = new EmployeeFoodEntity();
        unitEntities = new ArrayList<>();
        unitsName = new ArrayList<>();
    }

    public void init() {
        emptyPage();
        try {
            unitEntities = unitService.getALLUnit();
            for (UnitEntity u : unitEntities
                    ) {
                unitsName.add(u.getName());
            }
        } catch (BaseException e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));
        }
    }

    public void insert() {
        if (Objects.nonNull(insertEmployeeFood)) {
            try {
                EmployeeEntity employeeEntity =new EmployeeEntity();
                employeeEntity = unitService.searchEmployee(insertEmployeeFood.getPersonalCode().toString());
                if (Objects.isNull(employeeEntity)) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "کد پرسنلی باید دارای مقدار باشد", ""));
                } else {
                    FoodEntity foodEntity = new FoodEntity();
                    foodEntity = unitService.searchFoodByDateAndMeal(insertEmployeeFood.getDate(), insertEmployeeFood.getMeal());
                    if (Objects.isNull(foodEntity)) {
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "تاریخ و وعدع ی روزانه باید دارای مقدار باشد", ""));

                    } else {
                        insertEmployeeFood.setEmployeeEntity(employeeEntity);
                        insertEmployeeFood.setFoodEntity(foodEntity);
                        unitService.insertEmployeeFood(insertEmployeeFood);
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "غذای کارمند با موفقیت ثبت گردید", ""));
                        emptyPage();
                        searchDate=null;
                        searchMeal=null;
                        personalCode=null;
                    }
                }
            } catch (BaseException e) {
//                e.printStackTrace();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));
            }
        }
    }

    public void searchEmployeeFoodforView() {
        try {
            if (Objects.isNull(searchDate) || Objects.isNull(searchMeal) || Objects.isNull(personalCode)){
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "تمامی فیلد ها باید پر شوند", ""));
            } else {
                searchEmployeeFood = unitService.searchEmployeeFood(searchDate, searchMeal,personalCode);
                searchDate=null;
                searchMeal=null;
                personalCode=null;
            }
        } catch (BaseException e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));
        }
    }

    public void searchEmployeeFoodforDelete() {
        try {
            if (Objects.isNull(delDate) || Objects.isNull(delMeal) || Objects.isNull(delPersoanalCode)){
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "تمامی فیلد ها باید پر شوند", ""));
            } else {
                editEmployeeFood = unitService.searchEmployeeFood(delDate, delMeal,delPersoanalCode);
                delDate=null;
                delMeal=null;
                delPersoanalCode=null;
            }
        } catch (BaseException e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));
        }
    }

    public void deleteEmployeeFood() {
        try {
            if (Objects.nonNull(editEmployeeFood)) {
                unitService.deleteEmployeeFood(editEmployeeFood);
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "غذای کارمند با موفقیت حذف گردید", ""));
                emptyPage();
                delDate=null;
                delMeal=null;
                delPersoanalCode=null;
                searchDate=null;
                searchMeal=null;
                personalCode=null;
            }
        } catch (BaseException e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "غذای کارمند با موفقیت حذف گردید", ""));

        }
    }

    public EmployeeFoodEntity getInsertEmployeeFood() {
        return insertEmployeeFood;
    }

    public void setInsertEmployeeFood(EmployeeFoodEntity insertEmployeeFood) {
        this.insertEmployeeFood = insertEmployeeFood;
    }

    public EmployeeFoodEntity getSearchEmployeeFood() {
        return searchEmployeeFood;
    }

    public void setSearchEmployeeFood(EmployeeFoodEntity searchEmployeeFood) {
        this.searchEmployeeFood = searchEmployeeFood;
    }

    public EmployeeFoodEntity getEditEmployeeFood() {
        return editEmployeeFood;
    }

    public void setEditEmployeeFood(EmployeeFoodEntity editEmployeeFood) {
        this.editEmployeeFood = editEmployeeFood;
    }

    public List<UnitEntity> getUnitEntities() {
        return unitEntities;
    }

    public void setUnitEntities(List<UnitEntity> unitEntities) {
        this.unitEntities = unitEntities;
    }

    public List<String> getUnitsName() {
        return unitsName;
    }

    public void setUnitsName(List<String> unitsName) {
        this.unitsName = unitsName;
    }

    public String getSearchMeal() {
        return searchMeal;
    }

    public void setSearchMeal(String searchMeal) {
        this.searchMeal = searchMeal;
    }

    public Date getSearchDate() {
        return searchDate;
    }

    public void setSearchDate(Date searchDate) {
        this.searchDate = searchDate;
    }

    public String getDelMeal() {
        return delMeal;
    }

    public void setDelMeal(String delMeal) {
        this.delMeal = delMeal;
    }

    public Date getDelDate() {
        return delDate;
    }

    public void setDelDate(Date delDate) {
        this.delDate = delDate;
    }

    public Long getPersonalCode() {
        return personalCode;
    }

    public void setPersonalCode(Long personalCode) {
        this.personalCode = personalCode;
    }

    public Long getDelPersoanalCode() {
        return delPersoanalCode;
    }

    public void setDelPersoanalCode(Long delPersoanalCode) {
        this.delPersoanalCode = delPersoanalCode;
    }
}
